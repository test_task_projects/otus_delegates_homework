namespace Otus_Delegates_HomeWork.Events;

public class FilesFinder
{
    public event EventHandler<FileEventArgs>? FileFound;

    public bool CancelSearch { get; set; }

    public void ClearEvents()
    {
        if (FileFound == null) return;

        foreach (var eventAction in FileFound.GetInvocationList())
        {
            FileFound -= (EventHandler<FileEventArgs>)eventAction;
        }
    }

    public void FindFiles(string directoryPath)
    {
        try
        {
            if (!Directory.Exists(directoryPath)) return;
            CancelSearch = false;
            var files = Directory.GetFiles(directoryPath);
            foreach (var filePath in files)
            {
                FileFound?.Invoke(this, new FileEventArgs(Path.GetFileName(filePath)));
                if (CancelSearch)
                {
                    break;
                }
            }
        }
        catch (Exception e)
        {
            Console.WriteLine(e.Message);
        }
    }
}