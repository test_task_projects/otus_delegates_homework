namespace Otus_Delegates_HomeWork.Data;

public class TestClass
{
    public string Name { get; set; }
    
    public int A { get; set; }
    //
    public int B { get; set; }

    public override string ToString()
    {
        return $"{Name}:{A * B}";
    }
}