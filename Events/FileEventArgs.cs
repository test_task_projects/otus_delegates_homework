namespace Otus_Delegates_HomeWork.Events;

public class FileEventArgs:EventArgs
{
    public readonly string FileName;

    public FileEventArgs(string fileName)
    {
        FileName = fileName;
    }
}