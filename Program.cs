﻿using Otus_Delegates_HomeWork.Data;
using Otus_Delegates_HomeWork.Events;
using Otus_Delegates_HomeWork.Extensions;

namespace Otus_Delegates_HomeWork;

class Program
{
    static void Main(string[] args)
    {
        var rnd = new Random();
        var list = new List<TestClass>();
        for (int i = 0; i < 10; i++)
        {
            list.Add(new TestClass()
            {
                Name = $"Test-{i}",
                A = rnd.Next(1, 20),
                B = rnd.Next(2, 10)
            });
            Console.WriteLine(list[i].ToString());
        }

        Console.WriteLine($"MaxItem = {list.GetMax(testClass => testClass.A * testClass.B)}");


        var fileFinder = new FilesFinder();

        fileFinder.FileFound += OnFileFoundHandler;

        fileFinder.FindFiles(Environment.CurrentDirectory);
    }

    private static void OnFileFoundHandler(object? sender, FileEventArgs e)
    {
        var filesFinder = (sender as FilesFinder);
        filesFinder.CancelSearch = false;
        
        Console.WriteLine($"File found:{e.FileName}");
        //filesFinder.ClearEvents();
    }
}