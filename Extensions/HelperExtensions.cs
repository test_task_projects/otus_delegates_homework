using System.Collections;

namespace Otus_Delegates_HomeWork.Extensions;

public static class HelperExtensions
{
    public static T GetMax<T>(this IEnumerable<T> collection, Func<T, float> convertToNumbers) where T : class
    {
        T maxItem = default;
        float maxValue = float.NegativeInfinity;
        foreach (var item in collection)
        {
            var checkValue = convertToNumbers(item);
            if (checkValue > maxValue)
            {
                maxValue = checkValue;
                maxItem = item;
            }
        }

        return maxItem;
    }
}